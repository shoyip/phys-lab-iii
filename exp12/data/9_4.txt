ANALOG
Ch 1 Scale 2.00V/, Pos 0.0V, Coup DC, BW Limit Off, Inv Off, Imp 1M Ohm
     Probe 1.0000000 : 1, Skew 0.0s

TRIGGER
Sweep Mode Auto, Coup DC, Noise Rej Off, HF Rej Off, Holdoff 40.0ns
Mode Off

HORIZONTAL
Mode Roll, Ref Center, Main Scale 200.0ms/, Main Delay -1.000000000000s

ACQUISITION
Mode Normal, Realtime On, Vectors On, Persistence Off

MEASUREMENTS
Frequency(1), Cur 360Hz
Pk-Pk(1), Cur 5.4V
Std Dev(1), Cur 2.519V

