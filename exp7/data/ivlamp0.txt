ANALOG
Ch 1 Scale 500mV/, Pos 0.0V, Coup DC, BW Limit Off, Inv Off, Imp 1M Ohm
     Probe 1.0000000 : 1, Skew 0.0s
Ch 2 Scale 200mV/, Pos 0.0V, Coup DC, BW Limit Off, Inv Off, Imp 1M Ohm
     Probe 1.0000000 : 1, Skew 0.0s

TRIGGER
Sweep Mode Auto, Coup DC, Noise Rej Off, HF Rej Off, Holdoff 40.0ns
Mode Off

HORIZONTAL
Mode Roll, Ref Center, Main Scale 20.00s/, Main Delay -100.000000000000s

ACQUISITION
Mode High Res, Realtime On, Vectors On, Persistence Off

