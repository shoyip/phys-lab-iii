plot "data.dat" u 1:2 w lp

pause -1


set xlabel "Time ({/Symbol m}s)"
set ylabel "Voltage (V)"

plot "data.dat" u ($1*1e6):2 w lp

pause -1


set xrange [0:5]
plot "data.dat" u ($1*1e6):2 w lp

pause -1


f(x) = Vo + A*exp(-x/tau)
Vo = -3
A = 5
tau = 1.5

plot [0.5:4] "data.dat" u ($1*1e6):2 w lp, f(x) w l lw 2 ; pause -1


fit [0.1:4] f(x) "data.dat" u ($1*1e6):2 via Vo,A,tau
plot [0.1:4] "data.dat" u ($1*1e6):2 w lp, f(x) w l lw 2 ; pause -1

plot [-0.5:6] "data.dat" u ($1*1e6):2 w lp, f(x) w l lw 2 ; pause -1


plot [0.1:4] "data.dat" u ($1*1e6):($2-f($1*1e6)) w p ; pause -1

plot [0.1:1.5] "data.dat" every 5 u ($1*1e6):($2-f($1*1e6)) w p ; pause -1

g(x) = k

fit [0.1:5] g(x) "data.dat" every 5 u ($1*1e6):($2-f($1*1e6)) via k

fit [0.1:4] f(x) "data.dat" u ($1*1e6):2:(0.016) via Vo,A,tau
plot [0.1:4] "data.dat" u ($1*1e6):2 w lp, f(x) w l lw 2 ; pause -1
