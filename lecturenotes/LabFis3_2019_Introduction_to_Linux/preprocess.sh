#!/bin/bash

cat scope_0.csv | tail -n +3 | awk '{split($0,x,","); print x[1]"\t"x[2]}' > decay_0.dat
cat scope_1.csv | tail -n +3 | awk '{split($0,x,","); print x[1]"\t"x[2]}' > decay_1.dat
cat scope_2.csv | tail -n +3 | awk '{split($0,x,","); print x[1]"\t"x[2]}' > decay_2.dat
cat scope_3.csv | tail -n +3 | awk '{split($0,x,","); print x[1]"\t"x[2]}' > decay_3.dat
cat scope_4.csv | tail -n +3 | awk '{split($0,x,","); print x[1]"\t"x[2]}' > decay_4.dat

cat decay_0.dat | awk '{print $1}' > time_axis.dat

cat decay_0.dat | awk '{print $2}' > voltage_0.dat
cat decay_1.dat | awk '{print $2}' > voltage_1.dat
cat decay_2.dat | awk '{print $2}' > voltage_2.dat
cat decay_3.dat | awk '{print $2}' > voltage_3.dat
cat decay_4.dat | awk '{print $2}' > voltage_4.dat

paste time_axis.dat voltage_0.dat voltage_1.dat voltage_2.dat voltage_3.dat voltage_4.dat > data.dat
