ANALOG
Ch 1 Scale 1.40V/, Pos 0.0V, Coup DC, BW Limit Off, Inv Off, Imp 1M Ohm
     Probe 1.0000000 : 1, Skew 0.0s
Ch 2 Scale 74mV/, Pos 0.0V, Coup DC, BW Limit Off, Inv Off, Imp 1M Ohm
     Probe 1.0000000 : 1, Skew 0.0s

TRIGGER
Sweep Mode Auto, Coup DC, Noise Rej Off, HF Rej Off, Holdoff 40.0ns
Mode Edge, Source E, Slope Rising, Level 10mV

HORIZONTAL
Mode Normal, Ref Center, Main Scale 240.0us/, Main Delay 0.0s

ACQUISITION
Mode Normal, Realtime On, Vectors On, Persistence Off

