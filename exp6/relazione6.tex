\documentclass[a4paper]{article}
\usepackage[]{graphicx}
\usepackage[italian]{babel}
\usepackage{siunitx}
\usepackage[]{amsmath}
\usepackage{color}
\usepackage{adjustbox}
\sisetup{separate-uncertainty = true}

\title{Esperienza 6: Oscillatori}
\author{Chiara Trebo \and Shoichi Yip \and Niccolò Zen}
\date{}

\begin{document}
\maketitle

\section{Introduzione}

\subsubsection*{Componenti}

\begin{itemize}
	\item tre op-amp
\end{itemize}

\subsection{Oscillatore in quadratura smorzato}

Montiamo un circuito come quello raffigurato in Figura~\ref{fig:qo1}.

\begin{figure}[h]
	\centering
	\includegraphics[width=.8\textwidth]{circuit1.pdf}
	\caption{Circuito dell'oscillatore in quadratura}
	\label{fig:qo1}
\end{figure}

Il circuito è composto da due integratori in serie ed un amplificatore
invertente (amplificatore a guadagno $-1$), con il pin
invertente collegato all'uscita del secondo integratore e il pin non-invertente
collegato invece all'uscita del primo. Se indichiamo con $x$ la tensione in
entrata nel primo integratore abbiamo la seguente espressione del guadagno open
loop:
\begin{equation}
	\tilde G_{ol} = \frac{1}{s\tau} \frac{2 R_1}{R_1 + R_2} +
	\frac{1}{s^2 \tau^2},
	\label{eq:gol}
\end{equation}
in cui i termini quadratici sono dovuti alla serie di integratori con
l'invertitore, mentre la parte restante deriva dalla presenza delle resistenze
$R_1$ e $R_2$. Antitrasformando l'Equazione~\ref{eq:gol} otteniamo
\begin{equation}
	\frac{d^2x}{dt^2} + \frac{2}{\tau} \frac{R_2}{R_1 + R_2}
	\frac{dx}{dt} + \frac{1}{\tau^2} x = 0.
	\label{eq:gol_lat}
\end{equation}

Abbiamo dunque un oscillatore armonico, con uno smorzamento dovuto al termine
$\frac{2R_1}{\tau(R_1+R_2)}$. Il guadagno closed loop è dunque
\begin{equation}
	\begin{aligned}
		\tilde G_{cl} &= \frac{1}{1+\tilde G_{ol}}\\
		&= \frac{(R_1 + R_2) s^2 \tau^2}{s^2 \tau^2 (R_1 + R_2) + 2 R_1 s \tau +
		R_1 + R_2}.
	\end{aligned}
	\label{eq:gcl}
\end{equation}

\subsubsection{Stabilità con $\tilde G_{ol}$}

Possiamo studiare la stabilità del sistema direttamente controllando le
posizioni dei poli di $\tilde G_{cl}$ (Equazione~\ref{eq:gcl}): se studiamo gli
zeri del denominatore troviamo le soluzioni per
\begin{equation}
	s = \frac{-R_1 \pm \sqrt{R_1^2 - \tau^2(R_1 + R_2)^2}}{\tau^2(R_1 + R_2)}.
	\label{eq:poles}
\end{equation}

Le soluzioni descritte sono a parte reale negativa ($-\frac{R_2}{\tau^2
(R_1 + R_2)} < 0$), non appartengono al \emph{right half plane} e quindi il
sistema è stabile.

\subsubsection{Stabilità con Nyquist path}

Un modo alternativo per stabilire se il circuito preso in considerazione è
stabile o meno prevede l'uso del \emph{Nyquist path}: bisogna riparametrizzare
per la nostra $\tilde G_{ol}$ il cammino di Nyquist (Figura~\ref{fig:nyqp}).
Quello che otteniamo è la curva in Figura~\ref{fig:nyqf}.

\begin{figure}[h]
	\centering
	\begin{minipage}{0.3\textwidth}
		\centering
%		\includegraphics[width=.9\textwidth]{nyqp.pdf}
		\input{nyqp.pdf_tex}
		\caption{Cammino di Nyquist}
	\label{fig:nyqp}
	\end{minipage}\hfill
	\begin{minipage}{0.6\textwidth}
		\centering
		\includegraphics[width=.9\textwidth]{nyqf.pdf}
		\caption{Riparametrizzazione del cammino di Nyquist per $\tilde G_{ol}$}
	\label{fig:nyqf}
	\end{minipage}
\end{figure}

Il verso di percorrenza del cammino è antiorario, considerando che esso è chiuso
all'infinito da una parte di percorso che sta a destra di quello visibile in
Figura~\ref{fig:nyqf}. Poiché il cammino non include $-1$, quindi $\tilde
G_{ol}$ non ha zeri nel \emph{right hand plane} e il circuito è stabile.

\subsection{Oscillatore con compensazione}

Montiamo un circuito come quello in Figura~\ref{fig:circuit2}.

\begin{figure}[h]
	\centering
	\includegraphics[width=.7\textwidth]{circuit2.pdf}
	\caption{Circuito dell'oscillatore senza smorzamento}
	\label{fig:circuit2}
\end{figure}

Questo circuito è del tutto simile a quello preso in considerazione
precedentemente, ma ha un ramo ulteriore costituito da due resistenze da
\SI{1}{\mega\ohm} e due diodi che compensano lo smorzamento.

\label{subsec:compensation}

\section{Elaborazione dati}

Una volta impostato il circuito come in Figura~\ref{fig:qo1}, con $R_1 =
\SI{1}{\mega\ohm}$, $R_2 = \SI{100}{\ohm}$, $R = \SI{100}{\kilo\ohm}$ e
$C = \SI{10}{\nano\farad}$, e opportunamente alimentato, stimoliamo il sistema
collegando con un filo il pin invertente dell'op-amp nella configurazione di
ammplificatore invertente ad un'alimentazione. Dopo un breve transiente, vediamo
segnali come rappresentati in Figura~\ref{fig:signal1}.

\begin{figure}[h]
	\centering
	\includegraphics[width=.7\textwidth]{fig1.pdf}
	\caption{Esempio di segnale in uscita dall'oscillatore in
	Figura~\ref{fig:qo1}, con fit sullo smorzamento esponenziale}
	\label{fig:signal1}
\end{figure}

Catturiamo i segnali per diversi valori di $\epsilon = R_1/R_2$ (nel nostro caso
$10^{-1}$, $10^{-2}$, $10^{-3}$, $10^{-4}$ e $10^{-5}$), ed eseguiamo il fit alla funzione
\begin{equation}
	f(t) = A\, \exp\left(-\frac{t}{\tau_s}\right) \, \sin(\omega t + \phi) + c.
	\label{eq:fitfunc1}
\end{equation}

Questo ci dà i valori in Tabella~\ref{tab:expdec}.

\begin{table}
	\centering
	\[
	\begin{array}{c|c}
		\epsilon & 1/\tau_s \\
		\hline
		\SI{1e-1}{} & \SI{93.71(5)}{\hertz}\\
		\SI{1e-2}{} & \SI{12.455(6)}{\hertz}\\
		\SI{1e-3}{} & \SI{3.520(4)}{\hertz}\\
		\SI{1e-4}{} & \SI{2.637(4)}{\hertz}\\
		\SI{1e-5}{} & \SI{2.537(4)}{\hertz}
	\end{array}
\]
	\caption{Valori di $1/\tau_s$ del decadimento esponenziale per ogni
	configurazione}
	\label{tab:expdec}
\end{table}

Vediamo in Figura~\ref{fig:expdfit} i dati sperimentali del parametro $1/\tau_s$
dello smorzamento in funzione del parametro $\epsilon/(\epsilon+1)$, ed il fit
lineare effettuato su questo set di dati. Ipotizziamo che la correlazione
lineare tra $\epsilon/(\epsilon+1) = R_2 / (R_1 + R_2)$ e l'inverso del tempo
caratteristico di smorzamento $\tau_s$ sia dovuto alla presenza del termine
$\dot x$ nell'Equazione~\ref{eq:gol_lat}.

\begin{figure}[h]
	\centering
	\includegraphics[width=.7\textwidth]{fig5.pdf}
	\caption{Inverso del tempo caratteristico in funzione di
	$\epsilon/(1+\epsilon)$, per le diverse configurazioni di $\epsilon =
R_1/R_2$, con scala logaritmica sulle ascisse}
	\label{fig:expdfit}
\end{figure}

Nel limite $\epsilon\to 0$ vediamo come il tempo caratteristico $\tau_s$ tenda
all'infinito: questa è una previsione corretta, in quanto significa che per
$R_2\ll R_1$ (condizione in cui il ramo con le due resistenze $R_1$ ed
$R_2$ non esiste) non si ha più alcuno smorzamento. Studiando tuttavia il
circuito senza il ramo resistivo tuttavia si può rilevare il carattere instabile
di un tale eventuale circuito. In alternativa, si può costruire un circuito con
un ulteriore ramo con resistenze e diodi come descritto nella
Sezione~\ref{subsec:compensation} per compensare l'effetto dissipativo.

Dal fit lineare possiamo ricavare la pendenza, che corrisponde alla frequenza
del segnale e che ci aspettiamo, teoricamente, valere $f_{\text{teo}} = 1/(R C)
\approx \SI{159.155}{\radian\per\sec}$ (per i valori di resistenza e di capacità
abbiamo utilizzato quelli nominali indicati sulle componenti). La pendenza della
retta ricavata è di $f_{\text{exp}} = m/(2\pi)= \SI{149(4)}{\hertz}$, che
risulta compatibile entro $3\sigma$ con la $f_{\text{teo}}$.

Come si vede nelle Figure~\ref{fig:sincos1}, \ref{fig:sincos2},
\ref{fig:sincos3} e \ref{fig:sincos4}, al variare di $R_1$ ed $R_2$ varia
l'ampiezza del segnale $V_2$ rispetto a $V_1$, in uscita del secondo integratore. Le frequenze,
come si vede in Tabella~\ref{tab:sincosd}, cambiano a seconda della scelta di
$R$ e di $C$ poiché cambia $1/(2\pi R C)$ e i due
segnali, sempre come si vede in Tabella~\ref{tab:sincosd}, risultano essere
sempre in quadratura ($\Delta\phi = \pi/2 \approx \SI{1.5707963}{\radian}$).

Concludiamo dunque, tenendo a mente anche l'Equazione~\ref{eq:gol}, che
cambiando i parametri dell'amplificatore (ad esempio passando da guadagno
\num{-1} a guadagno \num{-2}), cioè cambiando i parametri $R_1$ ed $R_2$,
abbiamo una variazione nelle ampiezze di uscita, mentre abbiamo delle variazioni
nelle frequenze se modifichiamo i parametri dei due integratori ($R$ e
$C$). In tutti i casi lo sfasamento rimane uguale a $\pi/2$.

\begin{table}
	\centering
	\begin{adjustbox}{center}
	\[
	\begin{array}{c|cccc}
		\text{No.} & A_2/A_1 & \Delta\phi & \omega_1 & \omega_2\\
		\hline
		1 & \SI{1.02570(6)}{} & \SI{1.5727(1)}{\radian} &
		\SI{1001.593(15)}{\radian\per\sec} & \SI{1001.63(2)}{\radian\per\sec}\\
		2 & \SI{0.31048(7)}{} & \SI{1.5732(1)}{\radian} &
		\SI{3235.93(2)}{\radian\per\sec} & \SI{3235.00(3)}{\radian\per\sec}\\
		3 & \SI{1.31917(6)}{} & \SI{1.5728(1)}{\radian} &
		\SI{3130.17(2)}{\radian\per\sec} & \SI{3230.14(3)}{\radian\per\sec}\\
		4 & \SI{0.72271(5)}{} & \SI{1.5723(2)}{\radian} &
		\SI{1419.150(11)}{\radian\per\sec} & \SI{1419.120(11)}{\radian\per\sec}
	\end{array}
	\]
\end{adjustbox}
	\caption{Dati di ampiezza e sfasamento tra $V_2$ e $V_1$}
	\label{tab:sincosd}
\end{table}

\begin{figure}[p]
	\centering
	\begin{minipage}{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{fig6.pdf}
		\caption{Segnali in uscita dal primo e dal secondo integratore}
		\label{fig:sincos1}
	\end{minipage}
	\begin{minipage}[t]{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{fig7.pdf}
		\caption{Segnali in uscita dal primo e dal secondo integratore con $R$ e $C$
		differenti}
		\label{fig:sincos2}
	\end{minipage}
\end{figure}

\begin{figure}[p]
	\centering
	\begin{minipage}{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{fig8.pdf}
		\caption{Segnali in uscita dal primo e dal secondo integratore con $R$ e $C$
		differenti}
		\label{fig:sincos3}
	\end{minipage}
	\begin{minipage}[t]{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{fig9.pdf}
		\caption{Segnali in uscita dal primo e dal secondo integratore con $R$ e $C$
		differenti}
		\label{fig:sincos4}
	\end{minipage}
\end{figure}

\end{document}
