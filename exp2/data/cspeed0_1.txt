ANALOG
Ch 1 Scale 132mV/, Pos -574.250mV, Coup DC, BW Limit Off, Inv Off, Imp 1M Ohm
     Probe 1.0000000 : 1, Skew 0.0s

TRIGGER
Sweep Mode Auto, Coup DC, Noise Rej Off, HF Rej Off, Holdoff 40.0ns
Mode Edge, Source E, Slope Rising, Level 510mV

HORIZONTAL
Mode Normal, Ref Center, Main Scale 17.00ns/, Main Delay 33.800ns

ACQUISITION
Mode Normal, Realtime On, Vectors On, Persistence Off

