%% Script Esperienza 5
% Niccol� Zen, Shoichi Yip.

% Stiamo usando l'OP07

%% Dati 
close all; clc;

% Resistenze e capacit�
R1 = 97.054e3; %Ohm Fascetta rossa
dR1 = ( 0.01.*R1 + 0.001*100e3 ) ./ sqrt(12);
R2 = 98.808e3; %Ohm Fascetta blu
dR2 = ( 0.01.*R2 + 0.001*100e3 ) ./ sqrt(12);
C1 = 10.396e-9; %F Fascetta rossa
dC1 = ( 0.4.*C1 + 0.1*10e-9 ) ./ sqrt(12);
C2 = 10.043e-9; %F Fascetta blu
dC2 = ( 0.4.*C2 + 0.1*10e-9 ) ./ sqrt(12);
Rosc = 1e6; % Ohm
Cosc = 11e-12; %F
Tau = R1 * C1;

%% Import dati
Dati_1a = csvread('doublelp.csv',1,0);
freq_1a = Dati_1a(:,1);
Vin_1a = Dati_1a(:,2);
Vout_1a = Dati_1a(:,3);
phase_1a = Dati_1a(:,4);
t_scale_1a = Dati_1a(:,5);
Vin_scale_1a = Dati_1a(:,6);
Vout_scale_1a = Dati_1a(:,7);
w_1a = 2*pi*freq_1a;

Dati_1b = csvread('lpopamp1_1.csv',1,0);
freq_1b = Dati_1b(:,1);
Vin_1b = Dati_1b(:,2);
Vout_1b = Dati_1b(:,3);
phase_1b = Dati_1b(:,4);
t_scale_1b = Dati_1b(:,5);
Vin_scale_1b = Dati_1b(:,6);
Vout_scale_1b = Dati_1b(:,7);
w_1b = 2*pi*freq_1b;

Dati_1c = csvread('lpopamp2.csv',1,0);
freq_1c = Dati_1c(:,1);
Vin_1c = Dati_1c(:,2);
Vout_1c = Dati_1c(:,3);
phase_1c = Dati_1c(:,4);
t_scale_1c = Dati_1c(:,5);
Vin_scale_1c = Dati_1c(:,6);
Vout_scale_1c = Dati_1c(:,7);
w_1c = 2*pi*freq_1c;

Dati_2 = csvread('sk1.csv',1,0);
freq_2 = Dati_2(:,1);
Vin_2 = Dati_2(:,2);
Vout_2 = Dati_2(:,3);
phase_2 = Dati_2(:,4);
t_scale_2 = Dati_2(:,5);
Vin_scale_2 = Dati_2(:,6);
Vout_scale_2 = Dati_2(:,7);
w_2 = 2*pi*freq_2;

%% Bode Plot 1a
Incertezze_1a = incertosc(Vin_scale_1a,t_scale_1a,'V2',Vout_scale_1a);
dVin_1a = Incertezze_1a(1);
dVout_1a = Incertezze_1a(2);
dt_1a = Incertezze_1a(3);


Hteo_1a = 1 ./ ( -(w_1a.*Tau).^2 + 3i.*w_1a.*Tau + 1 );
modH_1a = Vout_1a ./ Vin_1a;
%dmodH_1a = 
angleH_1a = -phase_1a * pi / 180;

Hexp_1a = [modH_1a,angleH_1a];

Bode_1a_teo = bode_plot(freq_1a,Hteo_1a,'add_dB','col','g');
Bode_1a = bode_plot(freq_1a,Hexp_1a,'fig',Bode_1a_teo,'points','.','add_dB','col','b');

%% Bode plot 1b
Incertezze_1b = incertosc(Vin_scale_1b,t_scale_1b,'V2',Vout_scale_1b);
dVin_1b = Incertezze_1b(1);
dVout_1b = Incertezze_1b(2);
dt_1b = Incertezze_1b(3);


Hteo_1b = 1 ./ ( 1i.*w_1b.*Tau + 1 ).^2;
%Hteo_1b = 1 ./ ( 2i.*w_1b.*Tau + 1 - (w_1b.*Tau).^2 );
modH_1b = Vout_1b ./ Vin_1b;
%dmodH_1b = 
angleH_1b = -phase_1b * pi / 180;

Hexp_1b = [modH_1b,angleH_1b];

Bode_1b_teo = bode_plot(freq_1b,Hteo_1b,'add_dB','col','g');
Bode_1b = bode_plot(freq_1b,Hexp_1b,'fig',Bode_1b_teo,'points','.','add_dB','col','b');

%% Bode Plot 1c
Incertezze_1c = incertosc(Vin_scale_1c,t_scale_1c,'V2',Vout_scale_1c);
dVin_1c = Incertezze_1c(1);
dVout_1c = Incertezze_1c(2);
dt_1c = Incertezze_1c(3);

Hteo_1c = 1 ./ ( 1i.*w_1c.*Tau - 1 ).^2;
modH_1c = Vout_1c ./ Vin_1c;
%dmodH_1c = 
angleH_1c = phase_1c * pi / 180;

Hexp_1c = [modH_1c,angleH_1c];

Bode_1c_teo = bode_plot(freq_1c,Hteo_1c,'add_dB','col','g');
Bode_1c = bode_plot(freq_1c,Hexp_1c,'fig',Bode_1c_teo,'points','.','add_dB','col','b');

%% Bode Plot 2
Incertezze_2 = incertosc(Vin_scale_2,t_scale_2,'V2',Vout_scale_2);
dVin_2 = Incertezze_2(1);
dVout_2 = Incertezze_2(2);
dt_2 = Incertezze_2(3);

Hteo_2 = 1 ./ ( 1i.*w_1b.*Tau + 1 ).^2;
modH_2 = Vout_2 ./ Vin_2;
%dmodH_2 = 
angleH_2 = -phase_2 * pi / 180;

Hexp_2 = [modH_2,angleH_2];

Bode_2_teo = bode_plot(freq_2,Hteo_2,'add_dB','col','g');
Bode_2 = bode_plot(freq_2,Hexp_2,'fig',Bode_2_teo,'points','.','add_dB','col','b');

%% Import Impulsi 1a
Area = ( 10.11 - 4.16 )*1e-4 * 5;
Dati_1a = csvread('lpopamppulse.csv',5,0);
Vout_1a = Dati_1a(:,3);
Vin_1a = Dati_1a(:,2);
t_1a = Dati_1a(:,1);

%% Antitrasformata 1a
dVout_1a = 0.24./sqrt(12) * 310e-3 .* ones(size(Vout_1a));
% syms s
% F_1a = 1/(s^2 * Tau^2 + 3 * Tau * s + 1);
% iF_1a = ilaplace(F_1a)
t = t_1a - t_1a(1) .* ones(size(t_1a));
Ht_1a = t ./ (sqrt(5)*Tau) .* exp( - 3 ./ ( 2*Tau ) .* t ) .* sinh( sqrt(5) ./ (2*Tau) .* t );
figure()
hold on
grid on
TEO_A1 = errorbar(t,Ht_1a,0.*ones(size(t)));
A1 = errorbar(t,Vout_1a./5.7657,dVout_1a./2)
set(A1,'capsize',0)

%% Import Impulsi 1B
% 1a
Dati_1b = csvread('doublelpulse1_1.csv',5,0);
Vout_1b = Dati_1b(:,3);
Vin_1b = Dati_1b(:,2);
t_1b = Dati_1b(:,1);

%% Antitrasformata 1B

dVout_1b = 0.24./sqrt(12) * 310e-3 .* ones(size(Vout_1b));
dVin_1b = 0.24./sqrt(12) * 800e-3 .* ones(size(Vin_1b));
% syms s
% F_1a = 1/(s^2 * Tau^2 + 3 * Tau * s + 1);
% iF_1a = ilaplace(F_1a)
t = t_1b - t_1b(1) .* ones(size(t_1b));
Ht_1b = ( t .* exp( - t ./ Tau ) ) ./ Tau^2;
% figure()
% hold on
% grid on
% TEO_A1 = errorbar(t,Ht_1b,0.*ones(size(t)));
% %A1 = errorbar(t,Vout_1a,dVout_1a./2)
% set(A1,'capsize',0)


figure()
grid on
hold on
errorbar(t,Vin_1b,dVin_1b./2,'capsize',0)

%% Zout 1a
w = w_1a;
Zout_teo_1a = ( R1 + R2 + 1i .* w .* R1 .* C1 .* R2 ) ./ ( 1i .* w .* ( R1 .* C1 + R2 .* C2 + C2 ) - w .^2 .* R1 .* C1 .* ( R2 .* C2 - C2 ) ); 

Bode_1a_Zout_teo = bode_plot(freq_1a,Zout_teo_1a,'add_dB','col','r');
%Bode_1a_Zout = bode_plot(freq_1a,Hexp_1a,'fig',Bode_1a_Zout_teo,'points','.','add_dB','col','b');