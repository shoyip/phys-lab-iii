ANALOG
Ch 1 Scale 740mV/, Pos 2.60850V, Coup DC, BW Limit Off, Inv Off, Imp 1M Ohm
     Probe 1.0000000 : 1, Skew 0.0s
Ch 2 Scale 164mV/, Pos 555.475mV, Coup DC, BW Limit Off, Inv Off, Imp 1M Ohm
     Probe 1.0000000 : 1, Skew 0.0s

TRIGGER
Sweep Mode Normal, Coup DC, Noise Rej Off, HF Rej On, Holdoff 40.0ns
Mode Edge, Source Ch 1, Slope Rising, Level 1.16250V

HORIZONTAL
Mode Normal, Ref Center, Main Scale 1.320ms/, Main Delay 6.400000000ms

ACQUISITION
Mode Averaging, # Avgs 2, Realtime On, Vectors On, Persistence Off

MEASUREMENTS
RMS - N Cycles AC(1), Cur No edges
RMS - N Cycles AC(2), Cur No edges
Phase(1-2), Cur No edges
Frequency(1), Cur No edges

